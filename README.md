# base-js

## Базовый шаблон для проектирования приложений на js

### Включает в себя:

- ESlint (конфигурация airbnb)
- Prettier
- Pre-commit hook (lint-staged + husky)

----

### Настройка плагина ESlint для VScode

1.  ctrl + P
2.  В появившимся окне

        ext install dbaeumer.vscode-eslint

3.  Перезапустить редактор

----

### Настройка плагина Prettier для VScode

1.  ctrl + P
2.  В появившимся окне:

        ext install esbenp.prettier-vscode

3.  В User setting добавить следующие поля:

        "editor.formatOnPaste": false,
        "editor.formatOnSave": true,
        "[javascript]": {
                "editor.formatOnSave": true
        },
        "[html]": {
                "editor.formatOnSave": false
        },
        "[json]": {
                "editor.formatOnSave": false
        },
        "eslint.autoFixOnSave": true,
        "eslint.alwaysShowStatus": true,

4) Перезапусть редактор

----

### Оформление документации

Формат комментирования: JSDoc

Подробнее: http://usejsdoc.org/

Автоматическая конвертация JSDoc в MD:

- Устанавливаем jsdoc-to-markdown:

        npm install -g jsdoc-to-markdown

- Использование:

        jsdoc2md example.js

----

### Шаблоны

- React SPA https://gitlab.com/webdevtemp/base-js/tree/react
- React SSR https://gitlab.com/webdevtemp/base-js/tree/react-ssr-base
- React + Redux + Redux-saga https://gitlab.com/webdevtemp/base-js/tree/react-redux-saga
