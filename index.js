import Koa from 'koa';
import IO from 'koa-socket';

import config from './config';

const app = Koa();
const io = new IO();

io.attach(app);

io.on('connection', () => {
  console.log('[server] connected');
});

let usernames = [];

io.on('disconnect', ctx => {
  const { username } = ctx.socket;
  if (username) {
    console.log(`[server] disconnected: ${username}`);
    usernames = usernames.filter(u => u !== username);
  }
});

io.on('login', (ctx, { username }) => {
  console.log(`[server] login: ${username}`);
  usernames.push(username);
  ctx.socket.username = username;
  ctx.socket.emit('users.getList', { usernames });
  io.broadcast('users.login', { username });
});

io.on('logout', ctx => {
  const { username } = ctx.socket;
  if (username) {
    console.log(`[server] logout: ${username}`);
    usernames = usernames.filter(u => u !== username);
    delete ctx.socket.username;

    io.broadcast('users.logout', { username });
  }
});

const messages = [];
io.on('message', (ctx, { text }) => {
  console.log(`[server] message: ${text}`);
  const message = {
    id: messages.length,
    text,
    username: ctx.socket.username,
  };
  messages.push(message);

  io.broadcast('messages.new', { message });
});

app.listen(config.port, () => {
  console.log(`[server] ready on port ${config.port}`);
});
